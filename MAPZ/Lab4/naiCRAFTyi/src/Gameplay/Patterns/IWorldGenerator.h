#pragma once
#include "../World/Terrain.h"
#include "TreePlanter.h"

class IWorldGenerator
{
public: 
	virtual Craft::HeightMap GenerateWorld() = 0;
};

class PlainWorldGenerator : public IWorldGenerator
{
public:
	Craft::HeightMap  GenerateWorld()
	{
		
	}
};

class SmoothWorldGenerator : public IWorldGenerator
{
public:
	Craft::HeightMap GenerateWorld()
	{
		Craft::HeightMap heightMap;
		int octavesCount = 5;
		float frequency = 2048;
		int amplitude = 128;

		TreePlanter planter;
		OakTreeBuilder* builder = new OakTreeBuilder;
		heightMap = Craft::GenerateWorldHeightMap(Craft::CHUNK_SIZE, Craft::CHUNK_SIZE,
			frequency /= 2, amplitude /= 2);
		for (int i = 0; i < octavesCount; i++)
		{
			heightMap = Craft::AddHeightMaps(heightMap,
				Craft::GenerateWorldHeightMap(Craft::CHUNK_SIZE, Craft::CHUNK_SIZE,
					frequency /= 2, amplitude /= 2));
		}

	}
};

class RandomWorldGenerator : public IWorldGenerator
{
public:
	Craft::HeightMap  GenerateWorld()
	{

	}
};