#pragma once
#include <string>
#include <string_view>

class PlayerPositionMemento
{
	std::string m_state;
public:
	void SetState(std::string_view state) { m_state = state; }
	const std::string& GetState() const { return m_state; }
};

class Caretaker
{
	PlayerPositionMemento m_memento;
public:
	void Save(PlayerPositionMemento new_memento) { m_memento = new_memento; }
	const PlayerPositionMemento& Restore() const { return m_memento; }
};

