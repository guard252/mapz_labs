#pragma once
#include "TreeBuilder.h"

class TreePlanter
{
public:
	void PlantTree(TreeBuilder* builder)
	{
		builder->GenerateTree();
		builder->AddLeaves();
		builder->AddBody();
	}
};