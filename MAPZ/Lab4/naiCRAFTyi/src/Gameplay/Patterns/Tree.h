#pragma once

class Tree
{
public:
	enum class LeaveType
	{
		OakLeave,
		BirchLeave
	} m_leave_type;
	enum class BodyType
	{
		OakBody,
		BirchBody
	} m_body_type;
	void SetLeaveType(LeaveType type) { m_leave_type = type; }
	void SetBodyType(BodyType type) { m_body_type = type; }

};