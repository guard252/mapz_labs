#ifndef NAICRAFTYI_CHUNKMESH_H
#define NAICRAFTYI_CHUNKMESH_H
#include "../Engine/GLAbstraction/VAO.h"
#include "../Engine/GLAbstraction/VBLayout.h"
#include "../Engine/GLAbstraction/VBO.h"
#include "../Engine/GLAbstraction/IBO.h"
#include "../Engine/GLAbstraction/ArrayTexture.h"

#include "GLAbstraction/ShaderProgram.h"
#include "ChunkConstants.h"
#include "ChunkTypedefs.h"

namespace Craft
{

    class ChunkMesh
    {
        std::vector<GLfloat> mesh;
        std::vector<GLuint> indices;
        GL::VAO vao;
        GL::VBO vbo;
        GL::IBO ibo;
        GL::VBLayout layout;
        const GL::ShaderProgram& shader;
        glm::vec3 chunkShifting;

    private:
        int lastAddedVertices{0};
    public:
        ChunkMesh(const GL::ShaderProgram& shader, ChunkPosition pos);
        void GenerateMesh();
        void Render()const;
        void AddFace(const MeshQuad sq, BlockType type, BlockChunkPosition position);
    private:
        void SetTexIndex(MeshQuad sq, GLuint index);
        GLuint GetTexIndex(const MeshQuad sq, BlockType type);
        void MoveBlockToPosition(MeshQuad sq, BlockChunkPosition pos);
        void AddIndices();
    };
}

#endif //NAICRAFTYI_CHUNKMESH_H
