#ifndef NAICRAFTYI_TERRAIN_H
#define NAICRAFTYI_TERRAIN_H
#include "Chunk.h"
#include "../Patterns/BiomeFactory.h"
#include <map>
#include <array>
#include <unordered_map>
#include <cmath>
#include "../Engine/Others/Random.h"
#include "../Engine/Others/Math.h"
#include "TerrainGenertion.h"
#include <thread>
namespace Craft
{
    class Terrain
    {
        const int TERRAIN_PRIMARY_WIDTH = 16;
        const int TERRAIN_PRIMARY_LENGTH = 16;
        const int TERRAIN_DEPTH = 8;

        const GL::ShaderProgram& m_shader;
        ChunkTable m_chunks;
        HeightMap m_heightMap;
    private:
        void GenerateSmoothWorld();
        void GenerateColumn(int x, int z);

        Terrain(const GL::ShaderProgram& shader);
        Terrain(const Terrain&) = delete;
        Terrain operator = (const Terrain&) = delete;
    public:
        static auto& get_instance(const GL::ShaderProgram& shader)
        {
            static Terrain instance(shader);
            return instance;
        }
        
        ~Terrain();
        void CreateWorld();
        BlockType GetBlock(BlockWorldPosition position)const;
        BlockType GetBlock(ChunkPosition ch, BlockChunkPosition bl)const;
        void SetBlock(BlockWorldPosition pos, BlockType type);
        void SetBlockUnsafe(BlockWorldPosition pos, BlockType type);
        void SetBlockUnsafe(ChunkPosition ch, BlockChunkPosition bl, BlockType type);
        void SetColumn(BlockWorldPosition highestPoint, BlockType type);
        void PlaceTree(BlockWorldPosition pos);

        void RenderEntirely();

        void RenderChunksInRadius(const ChunkPosition& position, int radius);

        void GenerateBiomes(BiomeFactory* factory);

        void PlaceToWorld(Flora* flora, Fauna* fauna, Soil* soil) {}
    };
}

#endif //NAICRAFTYI_TERRAIN_H
