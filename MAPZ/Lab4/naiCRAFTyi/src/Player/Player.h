#pragma once
#include "Camera.h"
#include "Controller.h"
#include "../Gameplay/World/Chunk.h"
#include "../Gameplay/Patterns/Memento.h"
#include "../Gameplay/Patterns/ISerializable.h"
namespace Craft
{
	class Player : public ISerializable
	{
		std::string state;

		
		Camera camera;
		Controller controller;
		
	public: 
		void SetMemento(PlayerPositionMemento memento)
		{
			state = memento.GetState();
		}
		auto CreateMemento() const
		{
			PlayerPositionMemento memento;
			memento.SetState(state);
			return memento;
		}



		Player(GLFWwindow* window, const GL::ShaderProgram& blockShader);
		const Camera& GetCamera()const { return camera; }
		Controller& GetController() { return controller; }

		ChunkPosition GetChunkPosition()const
		{
			return GetChunkPos(camera.GetPosition());
		}
		std::string Serialize() const override { return ""; };

	};
}
