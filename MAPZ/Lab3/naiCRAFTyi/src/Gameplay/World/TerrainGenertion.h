#ifndef NAICRAFTYI_TERRAINGENERTION_H
#define NAICRAFTYI_TERRAINGENERTION_H

#include <vector>
#include "ChunkTypedefs.h"
#include "../Engine/Others/Math.h"

namespace Craft
{
    struct NoiseOptions
    {
        size_t amplitude;
        size_t frequency;
        size_t octavesCount;
    };

    HeightMap GenerateWorldHeightMap(int width, int length, float frequency, int amplitude);

    HeightMap AddHeightMaps(HeightMap f, HeightMap  s);

    //HeightMap MapToWorldHeight(HeightMap  heightMap, int lowBound, int hiBound);

    HeightMap CreateChunkColumnHeightMap(const ChunkPosition& chunkPosition, const NoiseOptions& options);
};


#endif //NAICRAFTYI_TERRAINGENERTION_H
