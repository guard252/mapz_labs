#ifndef NAICRAFTYI_CHUNKTYPEDEFS_H
#define NAICRAFTYI_CHUNKTYPEDEFS_H
#include "glm/glm.hpp"
#include "../Engine/Others/KeyFunctions.h"

namespace Craft
{
    using ChunkPosition = glm::ivec3;

    // This one is not a glm::uvec because if we need to
    // access blocks in other chunks, we will sometimes need
    // negative values
    using BlockChunkPosition = glm::ivec3;

    using BlockWorldPosition = glm::ivec3;

    using MeshQuad = float[24];

    class Chunk;

    using ChunkTable = std::unordered_map<ChunkPosition, Chunk*, KeyFunctions, KeyFunctions>;

    using RandVectors = std::unordered_map<glm::vec2, glm::vec2, KeyFunctions1, KeyFunctions1>;

    using HeightMap = std::unordered_map<std::pair<int, int>, int, KeyFunctions>;

}


#endif //NAICRAFTYI_CHUNKTYPEDEFS_H
