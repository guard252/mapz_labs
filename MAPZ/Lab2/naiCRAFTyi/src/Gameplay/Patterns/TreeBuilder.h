#pragma once
#include "Tree.h"

class TreeBuilder
{
protected:
	Tree* tree{ nullptr };
public:
	void GenerateTree()
	{
		tree = new Tree();
	}
	virtual void AddLeaves() = 0;
	virtual void AddBody() = 0;
	
};

class OakTreeBuilder : public TreeBuilder
{

	void AddLeaves() override
	{
		tree->SetLeaveType(Tree::LeaveType::OakLeave);
	}
	void AddBody() override
	{
		tree->SetBodyType(Tree::BodyType::OakBody);
	}
};

class BirchTreeBuilder : public TreeBuilder
{

	void AddLeaves() override
	{
		tree->SetLeaveType(Tree::LeaveType::BirchLeave);
	}
	void AddBody() override
	{
		tree->SetBodyType(Tree::BodyType::BirchBody);
	}
};
