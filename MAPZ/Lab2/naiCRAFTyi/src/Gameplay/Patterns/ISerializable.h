#pragma once
#include <string>

class ISerializable
{
public:
	virtual std::string Serialize() const = 0;
};

