#pragma once
#include "Biomes.h"

class BiomeFactory
{
public:
	virtual Soil* GenerateSoil() = 0;
	virtual Flora* GenerateFlora() = 0;
	virtual Fauna* GenerateFauna() = 0;
};

class ForestBiomeFactory
{
	virtual Soil* GenerateSoil()
	{
		return new GrassSoil;  
	}
	virtual Flora* GenerateFlora()
	{
		return new ForestFlora;
	}
	virtual Fauna* GenerateFauna()
	{
		return new CommonFauna;
	}
};

class DesertBiomeFactory
{
	virtual Soil* GenerateSoil()
	{
		return new SandSoil;
	}
	virtual Flora* GenerateFlora()
	{
		return new CactusFlora;
	}
	virtual Fauna* GenerateFauna()
	{
		return new NoFauna;
	}
};

class JungleBiomeFactory
{
	virtual Soil* GenerateSoil()
	{
		return new MossySoil;
	}
	virtual Flora* GenerateFlora()
	{
		return new PalmFlora;
	}
	virtual Fauna* GenerateFauna()
	{
		return new ExoticFauna;
	}
};