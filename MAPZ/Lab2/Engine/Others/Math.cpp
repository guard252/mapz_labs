#include "pch.h"
#include "Math.h"
/*
 * This function maps the dot product (0.0-1.0)
 * to the chunk position (0 - CHUNK_SIZE - 1)
 */
ENGINE_API int MapToBlockPosition(float val, int max)
{
    return fabs(val) * (max - 1);
}

ENGINE_API float MapToZeroOne(int val, int max)
{
    return float(val) / float(max);
}

ENGINE_API float MapTo(float destMin, float destMax, float srcMin, float srcMax, float toMap)
{
    return destMin + ((destMax - destMin) / (srcMax - srcMin)) * (toMap - srcMin);
}