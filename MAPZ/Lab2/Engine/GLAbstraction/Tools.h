#ifndef TOOLS_H
#define TOOLS_H

#include "glew/glew.h"

#include <iostream>
#include <iomanip>
#include "DLLDefines.h"

#if defined(__GNUC__) 
         #include <csignal>
         #define GLCall(x)\
         GLClearError();\
         x;\
         if(!GLLogCall()){\
         std::cout << " file: " << __FILE__ << "line: " << __LINE__ << " " << #x << std::endl;\
         raise(SIGTRAP);}
#elif defined(_MSC_VER)
#if defined _DEBUG
         #define GLCall(x)\
         GLClearError();\
         x;\
         if(!GLLogCall()){\
         std::cout << #x << std::endl;\
         __debugbreak();}
#else 
#define GLCall(x) x
#endif
#endif


ENGINE_API void GLClearError();
ENGINE_API bool GLLogCall();

#endif
