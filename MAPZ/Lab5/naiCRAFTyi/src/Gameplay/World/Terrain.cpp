#include "Terrain.h"
#include "../Patterns/TreePlanter.h"
//std::default_random_engine Random::engine{};

namespace Craft
{
	Terrain::Terrain(const GL::ShaderProgram& _shader) : m_shader{ _shader }
	{
		//std::thread world_generation_thread(&Terrain::CreateWorld, this);

		CreateWorld();
		//world_generation_thread.join();
	}

	Terrain::~Terrain()
	{
		for (auto& a : m_chunks)
		{
			delete a.second;
		}

	}

	void Terrain::CreateWorld()
	{
		for (int x = 1; x < TERRAIN_PRIMARY_WIDTH + 1; x++)
		{
			for (int y = 1; y < TERRAIN_DEPTH + 1; y++)
			{
				for (int z = 1; z < TERRAIN_PRIMARY_LENGTH + 1; z++)
				{
					ChunkPosition a(x, y, z);
					Chunk* c = new Chunk(m_shader, a, this);
					m_chunks.insert(std::make_pair(a, c));
				}
			}
		}
		GenerateSmoothWorld();
		for (int x = 1; x < TERRAIN_PRIMARY_WIDTH + 1; x++)
		{
			for (int y = 1; y < TERRAIN_DEPTH + 1; y++)
			{
				for (int z = 1; z < TERRAIN_PRIMARY_LENGTH + 1; z++)
				{
					ChunkPosition a(x, y, z);
					m_chunks.find(a)->second->CreateMesh();
				}
			}
		}


	}

	void Terrain::GenerateSmoothWorld()
	{
		int octavesCount = 5;
		float frequency = 2048;
		int amplitude = 128;
		TreePlanter planter;
		OakTreeBuilder* builder = new OakTreeBuilder;
		m_heightMap = GenerateWorldHeightMap(TERRAIN_PRIMARY_WIDTH * CHUNK_SIZE, TERRAIN_PRIMARY_LENGTH * CHUNK_SIZE,
			frequency /= 2, amplitude /= 2);
		for (int i = 0; i < octavesCount; i++)
		{
			m_heightMap = AddHeightMaps(m_heightMap,
				GenerateWorldHeightMap(TERRAIN_PRIMARY_WIDTH * CHUNK_SIZE, TERRAIN_PRIMARY_LENGTH * CHUNK_SIZE,
					frequency /= 2, amplitude /= 2));
		}
		for (int x = 1; x < TERRAIN_PRIMARY_WIDTH * CHUNK_SIZE + 1; x++)
		{
			for (int z = 1; z < TERRAIN_PRIMARY_LENGTH * CHUNK_SIZE - 1; z++)
			{
				SetColumn(BlockWorldPosition{ x, m_heightMap[{x,z}], z }, BlockType::DIRT);
				if (Random::GetInt(0, 100) < 1 && GetBlock({ x, m_heightMap[{x, z}], z }) == BlockType::DIRT)
				{
					planter.PlantTree(builder);
					PlaceTree({ x, m_heightMap[{x,z}], z });
				}
			}
		}


	}

	void Terrain::GenerateColumn(int x, int z)
	{
		NoiseOptions options{ 128, 2048, 5 };
		auto columnHeightMap = CreateChunkColumnHeightMap({ x, 0, z }, { options.amplitude /= 2, options.frequency /= 2 });

		for (int i = 0; i < options.octavesCount; i++)
		{
			columnHeightMap = AddHeightMaps(columnHeightMap, CreateChunkColumnHeightMap({ x, 0, z }, { options.amplitude /= 2 , options.frequency /= 2 }));
		}
		//TODO insert heightmap
		for (auto& element : columnHeightMap)
		{
			m_heightMap[element.first] = element.second;
		}
		for (int y = 1; y <= TERRAIN_DEPTH; y++)
		{
			ChunkPosition a(x, y, z);
			Chunk* c = new Chunk(m_shader, a, this);
			m_chunks.insert(std::make_pair(a, c));
		}

		BlockWorldPosition blockPos = ToWorldCoordinates({ x, 0, z }, { 0,0,0, });
		int i = blockPos.x;
		while (i != ((blockPos.x > 0) ? blockPos.x + CHUNK_SIZE : blockPos.x - CHUNK_SIZE))
		{
			int j = blockPos.z;
			while (j != ((blockPos.z > 0) ? blockPos.z + CHUNK_SIZE : blockPos.z - CHUNK_SIZE))
			{
				SetColumn(BlockWorldPosition{ i, columnHeightMap[{i,j}], j }, BlockType::DIRT);
				if (Random::GetInt(0, 100) < 1 && GetBlock({ i, columnHeightMap[{i,j}], j }) == BlockType::DIRT)
				{
					PlaceTree({ i, columnHeightMap[{i,j}], j });
				}
				if (blockPos.z > 0) j++;
				else j--;
			}
			if (blockPos.x > 0) i++;
			else i--;
		}

		for (int y = 1; y <= TERRAIN_DEPTH; y++)
		{
			ChunkPosition a(x, y, z);
			m_chunks.find(a)->second->CreateMesh();
		}
	}

	void Terrain::RenderEntirely()
	{
		for (auto& a : m_chunks)
		{
			a.second->Draw();
		}
	}

	void Terrain::RenderChunksInRadius(const ChunkPosition& playerPosition, int radius)
	{
		radius = abs(radius);
		int lower_x_bound = (playerPosition.x > 0 && playerPosition.x <= radius) ? playerPosition.x - radius - 1 : playerPosition.x - radius;
		int upper_x_bound = (playerPosition.x < 0 && playerPosition.x >= -radius) ? playerPosition.x + radius + 1 : playerPosition.x + radius;
		int lower_z_bound = (playerPosition.z > 0 && playerPosition.z <= radius) ? playerPosition.z - radius - 1 : playerPosition.z - radius;
		int upper_z_bound = (playerPosition.z < 0 && playerPosition.z >= -radius) ? playerPosition.z + radius + 1 : playerPosition.z + radius;

		for (int x = lower_x_bound; x <= upper_x_bound; x++)
		{
			if (x != 0)
				for (int z = lower_z_bound; z <= upper_z_bound; z++)
				{
					if (z != 0)
						for (int y = 1; y <= TERRAIN_DEPTH; y++)
						{
							if (y != 0)
							{
								if (m_chunks.find({ x, y, z }) != m_chunks.end())
								{
									m_chunks[{ x, y, z }]->Draw();
								}
								else
								{
									GenerateColumn(x, z);
									m_chunks[{ x, y, z }]->Draw();
								}

							}

						}
				}
		}
	}

	void Terrain::GenerateBiomes(BiomeFactory* factory)
	{
		auto flora = factory->GenerateFlora();
		auto fauna = factory->GenerateFauna();
		auto soil = factory->GenerateSoil();
		PlaceToWorld(flora, fauna, soil);
	}

	BlockType Terrain::GetBlock(BlockWorldPosition pos)const
	{
		ChunkPosition chunkPos = GetChunkPos(pos);


		if (auto currentChunk = m_chunks.find(chunkPos); currentChunk != m_chunks.end())
		{
			BlockChunkPosition blockPos = GetBlockLocalPos(pos);
			return currentChunk->second->GetBlock(blockPos.x, blockPos.y, blockPos.z);
		}
		return BlockType::AIR;
	}

	BlockType Terrain::GetBlock(ChunkPosition ch, BlockChunkPosition bl)const
	{
		if (bl.x == -1)
		{
			ch.x--; // 1, -1
			bl.x = CHUNK_SIZE - 1;
		}
		else if (bl.x == CHUNK_SIZE)
		{
			ch.x++;
			bl.x = 0;
		}
		if (bl.y == -1)
		{
			ch.y--;
			bl.y = CHUNK_SIZE - 1;
		}
		else if (bl.y == CHUNK_SIZE)
		{
			ch.y++;
			bl.y = 0;
		}
		if (bl.z == -1)
		{
			ch.z--;
			bl.z = CHUNK_SIZE - 1;
		}
		else if (bl.z == CHUNK_SIZE)
		{
			ch.z++;
			bl.z = 0;
		}

		if (auto currentChunk = m_chunks.find(ch); currentChunk != m_chunks.end())
		{
			return currentChunk->second->GetBlockUnsafe(bl.x, bl.y, bl.z);
		}

		return BlockType::AIR;

	}

	void Terrain::SetBlock(BlockWorldPosition pos, BlockType type)
	{
		ChunkPosition ch = GetChunkPos(pos);
		BlockChunkPosition bl = GetBlockLocalPos(pos);
		if (ChunkTable::iterator it = m_chunks.find(ch); it != m_chunks.end())
		{
			it->second->SetBlock(bl, type);
		}
	}

	void Terrain::SetBlockUnsafe(BlockWorldPosition pos, BlockType type)
	{
		ChunkPosition ch = GetChunkPos(pos);
		BlockChunkPosition bl = GetBlockLocalPos(pos);
		ChunkTable::iterator it;
		m_chunks.find(ch)->second->SetBlock(bl, type);
	}

	void Terrain::SetBlockUnsafe(ChunkPosition ch, BlockChunkPosition bl, BlockType type)
	{
		ChunkTable::iterator it;
		m_chunks.find(ch)->second->SetBlock(bl, type);
	}

	void Terrain::SetColumn(BlockWorldPosition highestPoint, BlockType type)
	{
		ChunkPosition ch = GetChunkPos(highestPoint);
		BlockChunkPosition bl = GetBlockLocalPos(highestPoint);
		do
		{
			if (m_chunks.find(ch) != m_chunks.end())
			{
				m_chunks.find(ch)->second->SetColumn(bl, type);
			}
			bl.y = CHUNK_SIZE - 1;
		} while (--ch.y && m_chunks.find(ch) != m_chunks.end());
	}

	void Terrain::PlaceTree(BlockWorldPosition pos)
	{
		int height = Random::GetInt(8, 12);
		int second_half = height / 2;
		int currentHeight = 0;
		while (currentHeight != height)
		{
			SetBlockUnsafe((pos), BlockType::WOOD);
			if (currentHeight > second_half)
			{
				SetBlock({ pos.x + 1, pos.y, pos.z }, BlockType::LEAVES);
				SetBlock({ pos.x - 1, pos.y, pos.z }, BlockType::LEAVES);
				SetBlock({ pos.x, pos.y, pos.z + 1 }, BlockType::LEAVES);
				SetBlock({ pos.x, pos.y, pos.z - 1 }, BlockType::LEAVES);
				SetBlock({ pos.x + 1, pos.y, pos.z + 1 }, BlockType::LEAVES);
				SetBlock({ pos.x + 1, pos.y, pos.z - 1 }, BlockType::LEAVES);
				SetBlock({ pos.x - 1, pos.y, pos.z + 1 }, BlockType::LEAVES);
				SetBlock({ pos.x - 1, pos.y, pos.z - 1 }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x + 2, pos.y, pos.z }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x + 2, pos.y, pos.z + 1 }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x + 2, pos.y, pos.z - 1 }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x, pos.y, pos.z + 2 }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x, pos.y, pos.z - 2 }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x - 2, pos.y, pos.z }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x - 2, pos.y, pos.z }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x - 2, pos.y, pos.z + 1 }, BlockType::LEAVES);
				if (Random::GetInt(0, 100) < 50)
					SetBlock({ pos.x - 2, pos.y, pos.z - 1 }, BlockType::LEAVES);


			}
			pos.y++;
			currentHeight++;
		}
		SetBlock({ pos.x, pos.y, pos.z }, BlockType::LEAVES);
		SetBlock({ pos.x + 1, pos.y, pos.z }, BlockType::LEAVES);
		SetBlock({ pos.x - 1, pos.y, pos.z }, BlockType::LEAVES);
		SetBlock({ pos.x, pos.y, pos.z + 1 }, BlockType::LEAVES);
		SetBlock({ pos.x, pos.y, pos.z - 1 }, BlockType::LEAVES);
	}
}