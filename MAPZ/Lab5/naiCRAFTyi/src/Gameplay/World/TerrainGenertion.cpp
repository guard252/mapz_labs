#include <map>

#include "glm/gtc/noise.hpp"
#include "TerrainGenertion.h"
#include "ChunkConstants.h"
#include "Coordinates.h"
#include "../Engine/Others/Random.h"
#include "Algorithms/SimplexNoise.h"
//ENGINE_API std::default_random_engine Random::engine{};

namespace Craft
{
	/*
	 * This function generates heightMaps for
	 * the part of a terrain
	 */
	HeightMap GenerateWorldHeightMap(int width, int length, float frequency, int amplitude)
	{
		HeightMap heightMap;
		/*for (int i = 0; i < width; i++)
		{
			heightMap[i] = std::vector<int>(length);
		}*/

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < length; y++)
			{
				float value = SimplexNoise::noise((double)x / frequency, (double)y / frequency);
				heightMap[{x, y}] = static_cast<int>(std::abs(MapTo(0, amplitude, 0, 1, value)));
			}
		}
		return heightMap;

	}



	HeightMap CreateChunkColumnHeightMap(const ChunkPosition& chunkPosition, const NoiseOptions& options)
	{
		//if (chunkPosition.x == -3 && chunkPosition.z == -1) __debugbreak();
		HeightMap heightMap;
		auto blockStartPosition = ToWorldCoordinates(chunkPosition, { 0 , 0, 0 });
		/* for (int i = 0; i < CHUNK_SIZE; i++)
		 {
			 heightMap[i] = std::vector<int>(CHUNK_SIZE);
		 }*/
		int startX = blockStartPosition.x;
		int startZ = blockStartPosition.z;
		int endX = (startX > 0) ? startX + CHUNK_SIZE : startX - CHUNK_SIZE;
		int endZ = (startZ > 0) ? startZ + CHUNK_SIZE : startZ - CHUNK_SIZE;
		int x = startX;
		while (x != endX)
		{
			int z = startZ;
			while (z != endZ)
			{
				float value = SimplexNoise::noise((double)x / options.frequency, (double)z / options.frequency);
				heightMap[{x, z}] = static_cast<int>(std::abs(MapTo(0, options.amplitude, 0, 1, fabs(value))));

				if (startZ < 0) z--;
				else z++;
			}
			if (startX < 0) x--;
			else x++;
		}
		return heightMap;
	}

	HeightMap AddHeightMaps(HeightMap f, HeightMap s)
	{
		HeightMap result = f;
		for (auto& a : result)
		{
			if (auto it = s.find(a.first); it != s.end())
				a.second += s.find(a.first)->second;
		}
		return result;
	}

	std::vector<std::vector<int>> MapToWorldHeight(std::vector<std::vector<float>> heightMap, int lowBound, int hiBound)
	{
		/* int width = heightMap.size();
		 int length = heightMap[0].size();
		 std::vector<std::vector<int>> result(width);

		 for (int x = 0; x < width; x++)
		 {
			 for (int y = 0; y < length; y++)
			 {
				 float value = glm::simplex(glm::vec2{x / strength, y / strength});
				 result[x][y] = std::abs(MapTo(57, 73, 0, 1, value));
			 }
		 }*/
		return std::vector<std::vector<int>>();
	}

}