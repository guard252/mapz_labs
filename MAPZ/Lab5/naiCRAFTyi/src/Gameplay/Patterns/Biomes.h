#pragma once

class Flora {};
class ForestFlora : public Flora {};
class CactusFlora : public Flora {};
class PalmFlora : public Flora {};

class Soil {};
class SandSoil : public Soil {};
class GrassSoil : public Soil {};
class MossySoil : public Soil {};

class Fauna {};
class CommonFauna : public Fauna {};
class NoFauna : public Fauna {};
class ExoticFauna : public Fauna {};