#include "Game.h"

namespace Craft
{
    Game::Game(GLFWwindow* _window): player(window, blockShader),
    window{_window} ,
    texPack{"textures/dirt.png",
            "textures/grass.png",
            "textures/grass_side.png",
            "textures/wood_top.png",
            "textures/wood_side.png",
            "textures/leaves.png",
            "textures/sand.png",
            "textures/cobblestone.png",
            "textures/bedrock.png"}
    {
    }

    int Game::Run()
    {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
        player.GetController().SetPrimarySettings(window);
        Terrain& terrain = Terrain::get_instance(blockShader);
        blockShader.SetUniform1i("slot", 0);
        while (!glfwWindowShouldClose(window))
        {
            glEnable(GL_DEPTH_TEST);
            GLCall(glClearColor(0.2f, 0.6f, 1.0f, 1.0f));
            GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

            player.GetController().HandleEvents(window);
            player.GetCamera().SendViewToShader();
            ChangeProjectionOnResize(window);
            blockShader.Bind();
            blockShader.SetUniformMatr4f("u_Proj", projection);

            terrain.RenderChunksInRadius(player.GetChunkPosition(), 3);

            glfwSwapBuffers(window);
            glfwPollEvents();
        }
        return 0;
    }

    void Game::ChangeProjectionOnResize(GLFWwindow* window)
    {
        int newHeight, newWidth;
        glfwGetWindowSize(window, &newWidth, &newHeight);
        if(width != newWidth || height != newHeight)
        {
            width = newWidth;
            height = newHeight;
            projection = glm::perspective(glm::radians(60.0f), (float)width / (float)height, 0.1f, 1000.0f);
            GLCall(glViewport(0, 0, width, height));
        }
    }
}
