#pragma once

#include <winsock2.h>
#include <string>
enum State
{
	LISTEN,
	ACCEPTED,
	RECEIVE
};

struct SocketState
{
	SOCKET id;			//socket handle
	State	state;			//receiving? 0 - LISTEN, 1 - ACCEPTED, 2 - RECEIVEING
	std::string buffer;
	std::string log_msg;
};